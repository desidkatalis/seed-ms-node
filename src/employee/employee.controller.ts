import hapi from '@hapi/hapi';
import employeeService from './employee.service';
import { Http } from '@dk/module-common';
import {
  EmployeeListResponseValidator,
  EmployeeResponseValidator,
  createEmployeeRequestValidator,
  updateEmployeeRequestValidator,
  deleteEmployeeRequestValidator
} from './employee.validator';

const getEmployees: hapi.ServerRoute = {
  method: Http.Method.GET,
  path: '/employees',
  options: {
    description: 'Get the list of employee',
    notes: 'Get the list of employee',
    tags: ['api', 'employees'],
    response: {
      schema: EmployeeListResponseValidator
    },
    handler: () => {
      return employeeService.getEmployees();
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.OK]: {
            description: 'Employees retrieved'
          }
        }
      }
    }
  }
};

const createEmployee: hapi.ServerRoute = {
  method: Http.Method.POST,
  path: '/employees',
  options: {
    description: 'Create new employee',
    notes: 'All information must valid',
    tags: ['api', 'employees'],
    response: {
      schema: EmployeeResponseValidator
    },
    validate: {
      payload: createEmployeeRequestValidator
    },
    handler: () => {
      return employeeService.getEmployees();
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.CREATED]: {
            description: 'Employee created'
          }
        }
      }
    }
  }
};

const updateEmployee: hapi.ServerRoute = {
  method: Http.Method.PUT,
  path: '/employees',
  options: {
    description: 'Update data employee',
    notes: 'All information must valid',
    tags: ['api', 'employees'],
    response: {
      schema: EmployeeResponseValidator
    },
    validate: {
      payload: updateEmployeeRequestValidator
    },
    handler: () => {
      return employeeService.getEmployees();
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.UPDATED]: {
            description: 'Employee updated'
          }
        }
      }
    }
  }
};

const deleteEmployee: hapi.ServerRoute = {
  method: Http.Method.DELETE,
  path: '/employees',
  options: {
    description: 'Delete data employee',
    tags: ['api', 'employees'],
    response: {
      schema: EmployeeResponseValidator
    },
    validate: {
      payload: deleteEmployeeRequestValidator
    },
    handler: () => {
      return employeeService.getEmployees();
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.OK]: {
            description: 'Employee deleted'
          }
        }
      }
    }
  }
};

const employeeController: hapi.ServerRoute[] = [
  getEmployees,
  createEmployee,
  updateEmployee,
  deleteEmployee
];
export default employeeController;
