import hapi from '@hapi/hapi';
import * as Joi from '@hapi/joi';

import {
  EmployeeValidator,
  EmployeeResponseValidator
} from './employee.validator';

export type IEmployee = Joi.extractType<typeof EmployeeValidator>;

export type IEmployeeResponse = Joi.extractType<
  typeof EmployeeResponseValidator
>;

export interface IEmployeeRequest extends hapi.Request {
  payload: IEmployee;
}
