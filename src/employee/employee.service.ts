import employeeRepository from './employee.repository';
import { IEmployee } from './employee.interface';
import { AppError } from '../errors/AppError';
import { ERROR_CODE } from '../common/errors';

const getEmployees = () => {
  return employeeRepository.getEmployee();
};

const createEmployee = async (employee: IEmployee) => {
  const existingEmployee = await employeeRepository.getFirstByName(
    employee.name
  );
  if (existingEmployee) {
    throw new AppError(ERROR_CODE.EMPLOYEE_NAME_EXISTED);
  }
  return employeeRepository.createEmployee(employee);
};

const updateEmployee = (employee: IEmployee, id: string) => {
  return employeeRepository.updateEmployee(employee, id);
};

const deleteEmployee = (id: string) => {
  return employeeRepository.deleteEmployee(id);
};

const employeeService = {
  getEmployees,
  createEmployee,
  updateEmployee,
  deleteEmployee
};

export default employeeService;
