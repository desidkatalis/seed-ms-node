import { EmployeeDocument, EmployeeModel } from './employee.model';
import { IEmployeeResponse, IEmployee } from './employee.interface';
import logger from '../logger';

const employeeDocumentToObject = (document: EmployeeDocument) =>
  document.toObject({ getters: true }) as IEmployeeResponse;

const employeeDocumentsToObject = (document: EmployeeDocument[]) =>
  document.map(employeeDocumentToObject);

const getEmployee = async () => {
  const documents = await EmployeeModel.find().exec();
  return employeeDocumentsToObject(documents);
};

const createEmployee = async (employee: IEmployee) => {
  const newEmployee = new EmployeeModel(employee);
  await newEmployee.save();
  logger.info('employee');
  return employeeDocumentToObject(newEmployee);
};

const updateEmployee = async (employee: IEmployee, id: string) => {
  const updateEmployee = await EmployeeModel.findOneAndUpdate(
    employee,
    id
  ).exec();
  return updateEmployee && employeeDocumentToObject(updateEmployee);
};

const deleteEmployee = async (id: string) => {
  const employee = await EmployeeModel.findOneAndDelete({ id }).exec();
  return employee && employeeDocumentToObject(employee);
};

const getFirstByName = async (name: string) => {
  const employee = await EmployeeModel.findOne({ name }).exec();
  return employee && employeeDocumentToObject(employee);
};

const employeeRepository = {
  getEmployee,
  createEmployee,
  getFirstByName,
  updateEmployee,
  deleteEmployee
};

export default employeeRepository;
