import { createLogger } from '@dk/module-logger';
import { Tracing } from '../src/common/constant';

const logger = createLogger({
  defaultMeta: {
    service: 'seed-ms-employee'
  },
  tracing: {
    tracerSessionName: Tracing.TRACER_SESSION,
    requestId: Tracing.TRANSACTION_ID
  }
});

export default logger;
